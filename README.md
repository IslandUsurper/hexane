# Purpose

A hex editor, written in Rust, for me (@IslandUsurper) to learn that language, test-driven development, and how to write software from scratch.
