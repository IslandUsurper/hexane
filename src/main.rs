use std::env::args_os;
use std::fs::File;
use std::io::Result as IOResult;
use std::io::Read;

fn dump(path: &str) -> IOResult<String> {
    let f = try!(File::open(path));
    Ok(f.bytes()
        .filter_map(|i| i.ok())
        .fold(
            String::new(),
            |a, b| a + format!("{:02X}", b).as_str()
        )
    )
}

pub fn main() {
    if let Some(os_path) = args_os().nth(1) {
        if let Some(path) = os_path.to_str() {
            match dump(&path) {
                Ok(output) => println!("{}", output),
                Err(_) => unimplemented!(),
            }
        }
    }
}
