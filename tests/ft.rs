use std::process::Command;

#[test]
fn open_a_file_and_view_its_contents() {
    // Edna finds a binary file on her computer.
    // Since it's not a text file, she opens it with hexane.
    let output = Command::new("target/debug/hexane")
                         .arg("tests/tst1")
                         .output()
                         .expect("Could not run hexane.");
    // The file is interpreted as hexadecimal and printed to her terminal.
    assert_eq!(String::from_utf8_lossy(&output.stdout), "31323334350A\n");
    // Satisified it is not a virus, Edna exits hexane.
    assert!(output.status.success());
}
